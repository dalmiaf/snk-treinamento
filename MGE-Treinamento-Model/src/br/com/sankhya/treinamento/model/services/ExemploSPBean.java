package br.com.sankhya.treinamento.model.services;

import java.rmi.RemoteException;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.util.BaseSPBean;
import br.com.sankhya.ws.ServiceContext;

/**
 * @ejb.bean name="ExemploSP" jndi-name="mge/treinamento/ejb/session/ExemploSP" type="Stateless" transaction-type="Container" view-type="remote"
 *
 * @ejb.transaction type="Supports"
 *
 * @ejb.util generate="false"
 */
public class ExemploSPBean extends BaseSPBean implements SessionBean {
	
	private SessionContext context;
	
	public void ejbActivate() throws EJBException, RemoteException {
	}
	
	public void ejbPassivate() throws EJBException, RemoteException {
	}
	
	public void ejbRemove() throws EJBException, RemoteException {
	}
	
	public void setSessionContext(SessionContext ctx) throws EJBException, RemoteException {
		this.context = ctx;
	}
	
	private void throwsMGEExceptionRollingBack(Throwable e) throws MGEModelException {
		context.setRollbackOnly();
		MGEModelException.throwMe(e);
	}
	
	/**
	 *  @ejb.interface-method tview-type="remote"
	 */
	public void validarCatalogo(ServiceContext sctx) throws MGEModelException {
		SessionHandle hnd = null;
		
		try {
			hnd = JapeSession.open();
			
		} catch(Exception e) {
			MGEModelException.throwMe(e);
			
		} finally {
			JapeSession.close(hnd);
		}
	}
	
	/**
	 *  @ejb.interface-method tview-type="remote"
	 *  @ejb.transaction type="Required"
	 */
	public void salvarDetalhesCatalogo(ServiceContext sctx) throws MGEModelException {
		SessionHandle hnd = null;
		
		try {
			hnd = JapeSession.open();
			
		} catch(Exception e) {
			throwsMGEExceptionRollingBack(e);
			
		} finally {
			JapeSession.close(hnd);
		}
	}
}
